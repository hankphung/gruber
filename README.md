# Gruber

a [Sails](http://sailsjs.org) application

## Install

You must have Nodejs and NPM running on your system. 
1. Clone this repo: `git clone git@bitbucket.org:hankphung/gruber.git gruber`
2. Run npm install `cd gruber && npm install`
3. Start `sails lift` or `node app.js`