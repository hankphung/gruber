var expect = require('chai').expect;
describe('Driver model', function() {

    describe('#create()', function() {
        it('should validate: reject on required missing', function(done) {
            Driver.create({}).exec(function(err, result) {
                expect(err).to.be.a('object');
                expect(err.code).to.equal('E_VALIDATION');
                done();

            });
        });
        it('should validate: success with valid values', function(done) {
            Driver.create({
                name: 'yo'
            }).exec(function(err, result) {
                if (err) done(err);
                else {
                    expect(result).to.be.a('object');
                    done();
                }
                //expect(err.code).to.equal('E_VALIDATION');
                //done();
            });
        })
    })
});
