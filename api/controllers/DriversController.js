/**
 * DriversController
 *
 * @description :: Server-side logic for managing drivers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

/**
 * @api {post} /drivers register new driver
 * @apiName Driver
 *
 * @apiParam {String} name Drivers name.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 Created
 *     {
 *     	 "id": [driverid]
 *       "name": "Doe"
 *     }
 *
 */
module.exports = {
	/**
 * @api {put} /driver/:id/locations Update driver location
 * @apiName Driver
 *
 * @apiParam {String} id Drivers unique ID.
 *
 */
	locations:function(req,res){
		Geo.updateDriverLocation(req.params.id,req.body.location,function(){
			res.json({
				id:req.params.id,
				location:req.body.location
			});
		});
		//Geo.addLocation(locationName, point, callBack)
	}
};

