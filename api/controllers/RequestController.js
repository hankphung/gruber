module.exports={
	create:function(req,res){
		requestInfo=req.body;
		var conditions={
			lng: parseFloat(requestInfo.location.lng) || 0,
			lat: parseFloat(requestInfo.location.lat) || 0,
			maxDistance: sails.config.maxScanDistance,
			limit: sails.config.scanLimit,
			query:{state: 'available'}
		}
		Drivers.findNear(conditions,function(e,r){
			if(e) return res.forBidden();
			res.json({
				data:r
			});
		})
	}
};