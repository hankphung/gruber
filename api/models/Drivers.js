/**
 * Driver.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
    autoUpdatedAt: false,
    autoCreatedAt: false,
    attributes: {
        name: {
            type: 'string',
            required: true
        },
        location: {
            type: 'json',
        },
        state: {
            type: 'string',
        //    defaultsTo: 'available'
        },
        toJSON: function() {
            var obj = this.toObject();
            if (obj.location) {
                location = obj.location
                obj.location = {
                    lng: location[0],
                    lat: location[1]
                }
            }
            return obj;
        }
    },
    findNear: function(condition, callback) {
        Drivers.native(function(err, collection) {
            if (err) return callback(err);

            collection.aggregate([{
                    "$geoNear": {
                        "near": {
                            "type": "Point",
                            "coordinates": [condition.lng, condition.lat]
                        },
                        "distanceField": "distance",
                        "maxDistance": condition.maxDistance,
                        "spherical": true,
                        limit: condition.limit || 5,
                        "query": condition.query || {}
                    }
                }, {
                    "$sort": { "distance": -1 } // Sort the nearest first
                }],
                function(e, places) {
                    return callback(e, places);
                });

            /*collection.geoNear({
                type: "Point",
                coordinates: [condition.lng, condition.lat]
            }, {
              
                maxDistance: condition.maxDistance * 1000.0,
                distanceMultiplier: 0.001,
                spherical: true
            }, function(e, places) {
                return callback(e, places);
            });
*/

        });
    },
    /*toJSON:function(){

    }*/
};
