var redis = require('redis'),
    throng = require('throng'),

    pub = redis.createClient();

var sub = redis.createClient()
sub.on("message", function(channel, message) {

    data = JSON.parse(message);
    Drivers.update({ id: data.id }, {location: data.location}, function(e, d) {
        sails.log.error("cannot update driver",e,d);
    });
});

sub.subscribe("driver_location");

module.exports = {
    updateDriverLocation: function(id, point, cb) {
        var messageData = {
            id: id,
            location: [point.lng, point.lat],
        }
        pub.publish("driver_location", JSON.stringify(messageData));
        cb();
    }
}
